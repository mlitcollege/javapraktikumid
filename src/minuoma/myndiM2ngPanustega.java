package minuoma;

import praktikum6.Meetodid;

public class myndiM2ngPanustega {

	public static void main(String[] args) {
		
		// 0 - kull, 1 - kiri
		int kasutajaRaha = 25;
		
		while (kasutajaRaha > 0) {
			System.out.println("Sul on raha: " + kasutajaRaha);
			int maxPanus = Math.min(25, kasutajaRaha);
			int panus = Meetodid.kasutajaSisestus("Sisesta panus (max " + maxPanus + ")", 1, maxPanus);
			int myndiVise = Meetodid.suvalineArv(0, 1);
			if (1 == myndiVise) {
				System.out.println("Arvasid ära!");
				kasutajaRaha += panus * 2;
			} else {
				System.out.println("Mööda panid!");
				kasutajaRaha -= panus;
			} 
		}
		System.out.println("raha otsas, mäng läbi");
	}

}
