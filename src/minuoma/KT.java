package minuoma;

public class KT {
/* ESIMENE --------------------------------------
 * Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d põhjal niisuguste elementide arvu, mis on rangelt väiksemad kõigi elementide aritmeetilisest keskmisest (aritmeetiline keskmine = summa / elementide_arv). 
 Write a method in Java to find the number of elements strictly less than arithmetic mean of all elements of a given array of real numbers d (arithmetic mean = sum_of_elements / number_of_elements).
 public static int allaKeskmise (double[] d)
 ----------------------
 public class Answer {

   public static void main (String[] args) {
      System.out.println (allaKeskmise (new double[]{0.}));
      // YOUR TESTS HERE
   }

   public static int allaKeskmise (double[] d) {
      return -1;  // YOUR PROGRAM HERE
   }

}

TEINE -----------------------------------
Koostage Java meetod, mis genereerib parameetrina etteantud n järgi niisuguse n korda n täisarvumaatriksi, mille iga elemendi väärtuseks on maksimaalne selle elemendi reaindeksist ja veeruindeksist (indeksid algavad nullist). 
Write a method in Java to generate an integer matrix of size n x n (n is a parameter of the method) elements of which are calculated by finding a maximum of the row index and the column index of the element (indices start from zero).
   public static int[][] muster (int n)
 --------------------------------  
   public class Answer {

   public static void main (String[] args) {
      int[][] res = muster (9);
   }

   public static int[][] muster (int n) {
      return null; // TODO!!! Your code here
   }

}
KOLMAS ----------------------------------------
Sportlase punktisumma arvutatakse üksikkatsetest saadud punktide summana, millest on maha võetud kahe halvima katse tulemused (üksikkatseid on rohkem kui kaks).
Kirjutada Java meetod, mis arvutab punktisumma üksikkatsete tulemuste massiivi põhjal.
Parameetriks olevat massiivi muuta ei tohi.
Sportsmans score is calculated as sum of points from different attempts and two worst attempts are not counted (there are more than two attempts).
Write a Java method to calculate the score if an array of points from all attempts is given.
Do not change the array given as parameter.
   public static int score (int[] points)
   -----------------------------
  public class Answer {

   public static void main (String[] args) {
      System.out.println (score (new int[]{4, 1, 2, 3, 0})); // 9
      // Your tests here
   }

   public static int score (int[] points) {
      return 0; // TODO!!! Your program here
   }

}
  NELJAS --------------------------
  Koostage Java meetod etteantud täisarvumaatriksi m veerumiinimumide massiivi leidmiseks (massiivi j-ndaks elemendiks on maatriksi j-nda veeru vähima elemendi väärtus). Arvestage, et m read võivad olla erineva pikkusega. 
Write a method in Java to find the array of minimums of columns of a given matrix of integers m (j-th element of the answer is the minimum of elements of the j-th column in the matrix). Rows of m might be of different length.
   public static int[] veeruMinid (int[][] m)
   -----------------------------
   public class Answer {

   public static void main (String[] args) {
      int[] res = veeruMinid (new int[][] { {1,2,6}, {4,5,3} }); // {1, 2, 3}
      // YOUR TESTS HERE
   }

   public static int[] veeruMinid (int[][] m) {
      // TODO!!!    YOUR PROGRAM HERE
      return null;
   }

}
----------------------------
javac Answer.java
java Answer 
*/
}
