package praktikum2;

import lib.TextIO;

public class ArvudeKorrutis {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// defineerime muutujad
		int arv1;
		int arv2;
		int korrutis;

		// kysime kasutajalt 2 arvu
		System.out.println("Palun sisesta kaks arvu");
		// väärtustame muutujad
		arv1 = TextIO.getlnInt();
		arv2 = TextIO.getInt();
		// arvutame korrutise
		korrutis = arv1 * arv2;
		// väljastame tulemuse
		System.out.println("Nende arvude korrutis on" + korrutis);
	}

}
