package praktikum5;

public class KolmegaJaguvad {

	public static void main(String[] args) {
		//1
		for (int i = 0; i <= 30; i++) {
			int arv = 30 - i;
			if (arv % 3 == 0) {
				System.out.print(arv + " ");
			}
		}
		
		System.out.println();
		//2
		for (int i = 30; i >= 0; i -= 3) {
			System.out.print(i + " ");
		}

	}

}
