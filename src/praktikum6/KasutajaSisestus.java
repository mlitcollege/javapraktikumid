package praktikum6;

import lib.TextIO;

public class KasutajaSisestus {

	public static void main(String[] args) {
		int hinne = kasutajaSisestus(1, 5);
		System.out.println("kasutaja sisestas: " + hinne);

	}

	private static int kasutajaSisestus(int min, int max) {
		while (true) {
			System.out.println("Palun sisesta arv vahemikus: " + min + " kuni " + max);
			int sisestus = TextIO.getlnInt();
			if (sisestus > min && sisestus <= max) {
				return sisestus;
			} else {
				System.out.println("See arv pole õiges vahemikus");
			}
		}
	}
}
