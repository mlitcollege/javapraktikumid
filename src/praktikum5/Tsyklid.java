package praktikum5;

public class Tsyklid {

	public static void main(String[] args) {
		// if - kui tingimus t�idetud, siis minnakse tsykli l�ppu 
		// (teeb �ra - l�heb v�lja)
		if (true) { 
			System.out.println("Tingimus vastab t�ele");
		}
		// while - kui tingimus t�idetud, siis minnakse algusesse seda uuesti kontrollima 
		// (teeb �ra - j��b tiirutama)
		// break; - tsykli t�� saab l�petada
		int i = 0;
		while (i < 3) {
			System.out.println("Tingimus vastab t�ele (while)");
			i++; // i = i + 1
		}
		
		for (int k = 0; k < 10; k++) {
			System.out.println("for tsykkel, i: " + i);
		}

	}

}
