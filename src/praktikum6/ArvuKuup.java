package praktikum6;

import lib.TextIO;

public class ArvuKuup {

	public static void main(String[] args) {

		System.out.println("Sisesta arv:");
		int arv = TextIO.getlnInt();
		int arvKuubis = kuup(arv);
		System.out.println("Sisestatud arvu kuup on: ");
		System.out.println(arvKuubis);
	}

	private static int kuup(int number) {
		// return number * number * number;
		// arvu astendamist saab tähistada Math.pow(); abil
		return (int) Math.pow(number, 3);
	}

}
